﻿
damerian0001 = {
	name = "Marion"
	dna = 1_marion_firstborn
	dynasty = dynasty_silmuna	#Silmuna
	religion = "cult_of_the_dame"
	culture = "damerian"
	mother = 13	#Auci
	father = 20 #Munas
	
	diplomacy = 13
	martial = 10
	stewardship = 4
	intrigue = 9
	learning = 2
	prowess = 13	#hes a good enough knight to fight sorcerer king or be selected
	trait = education_martial_3
	trait = reckless
	trait = gregarious
	trait = ambitious
	trait = impatient
	trait = beauty_good_2
	trait = race_half_elf
	trait = fecund
	trait = magical_affinity_2

	1001.8.19 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			set_sexuality = bisexual
			if = {
				limit = {
					has_game_rule = enable_marked_by_destiny
				}
				add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
			}
		}
	}
	
	1002.1.1 = {
		give_nickname = nick_firstborn
	}

	1020.10.31 = {	#Battle of Trialmount
		effect = {
			set_relation_friend = character:36	#Ottrac the Wexonard
			set_relation_friend = character:34	#Clarimonde of Oldhaven
		}
	}
	
	1021.10.3 = {
		set_artifact_rarity_famed = yes
		create_artifact = {
			name = silverbound_axe_of_dameria_name
			description = silverbound_axe_of_dameria_description
			type = pedestal
			visuals = axe_court
			wealth = scope:wealth
			quality = scope:quality
			history = {
				type = inherited
				date = 1021.10.3
			}
			modifier = silverbound_axe_of_dameria_modifier
			save_scope_as = newly_created_artifact
		}
		scope:newly_created_artifact = {
			set_variable = { name = historical_unique_artifact value = yes }
		}
	}
}


dameris0001 = { # Canrec Dameris
	name = "Canrec"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_diplomacy_3
	trait = ambitious
	trait = generous
	trait = chaste
	trait = diplomat
	
	863.7.15 = {
		birth = yes
	}
	
	890.3.3 = {
		add_spouse =
	}
	
	915.10.30 = {
		death = "915.10.30"
	}
}

dameris0002 = { # Aucanus Dameris
	name = "Aucanus"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_stewardship_2
	trait = diligent
	trait = cynical
	trait = vengeful
	
	father = dameris0001
	
	893.4.18 = {
		birth = yes
	}
	
	910.8.1 = {
		add_spouse = lorentis0004
	}
	
	940.2.2 = {
		death = "955.10.2"
	}
}

dameris0003 = { # Arrel Dameris
	name = "Arrel"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = twin
	
	father = dameris0002
	mother = lorentis0004
	
	912.3.24 = {
		birth = yes
	}
	
	955.10.2 = {
		death = {
			death_reason = death_ill
		}
	}
}

dameris0004 = { # Rogec Dameris
	name = "Rogec"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = twin
	
	father = dameris0002
	mother = lorentis0004
	
	912.3.24 = {
		birth = yes
	}
	
	948.7.7 = {
		death = {
			death_reason = death_battle
		}
	}
}

dameris0005 = { # Josern Dameris, second husband of Reanna II of Lorent
	name = "Josern"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = carnetori
	
	trait = race_human
	trait = education_learning_2
	trait = patient
	trait = temperate
	trait = content
	
	father = dameris0004
	mother = damerian70072
	
	935.10.27 = {
		birth = yes
	}
	
	978.10.20 = {
		add_matrilineal_spouse = lorentis0006
	}
	
	997.3.3 = {
		death = "997.3.3"
	}
}

dameris0006 = { # Aucanna Dameris
	name = "Aucanna"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = content
	trait = generous
	trait = deceitful
	
	father = dameris0004
	mother = damerian70072
	
	948.10.4 = {
		birth = yes
	}
}

24 = {
	name = "Marven"
	dynasty_house = house_dameris #Dameris
	religion = "cult_of_the_dame"
	culture = "carnetori"
	father = 25 #Galien Dameris
	
	trait = education_martial_2
	trait = reckless
	trait = impatient
	trait = arrogant
	trait = brave
	trait = physique_good_3
	trait = race_human
	
	980.2.4 = {
		birth = yes
	}
	
	998.2.1 = {
		death = "998.2.1"
	}
}

25 = {
	name = "Galien"
	dynasty_house = house_dameris #Dameris
	religion = "cult_of_the_dame"
	culture = "old_damerian"
	
	trait = education_learning_4
	trait = patient
	trait = zealous
	trait = forgiving
	trait = theologian
	trait = beauty_good_3
	trait = race_human
	
	father = dameris0003
	mother = damerian70071
	
	930.6.8 = {
		birth = yes
	}
	
	980.12.1 = {
		death = "980.12.1"
	}
}


7000 = { # Westyn I "the Elder" of Wesdam
	name = "Westyn"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = honest
	trait = impatient
	trait = diligent
	trait = education_stewardship_2
	
	father = 7003
	mother = 7004
	
	986.12.17 = {
		birth = yes
	}
	
	1005.2.5 = {
		add_spouse = 7001
	}
	
	1018.4.13 = {
		death = {
			death_reason = death_duel
			killer = 50018 # Trutgard of Marllin
		}
	}
}

7001 = { # Amelie, wife of Westyn "the Elder"
	name = "Amelie"
	dynasty_house = house_roilsard
	religion = court_of_adean
	culture = roilsardi
	female = yes
	
	trait = race_human
	trait = gregarious
	trait = patient
	trait = content
	trait = education_diplomacy_1
	
	father = 504
	
	988.4.11 = {
		birth = yes
	}
	
	1005.2.5 = {
		add_spouse = 7000
	}
}

7002 = { # Westyn II "the Younger" of Wesdam
	name = "Westyn"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = shy
	trait = humble
	trait = chaste
	trait = education_learning_2
	
	father = 7000
	mother = 7001
	
	1005.11.2 = {
		birth = yes
	}
}

7003 = { # Ricard I of Wesdam
	name = "Ricard"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = wrathful
	trait = cynical
	trait = stubborn
	trait = education_martial_3
	
	father = damerian7009
	
	964.6.13 = {
		birth = yes
	}
	
	985.11.26 = {
		add_spouse = 7004
	}
	
	1014.7.24 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

7004 = { # Cora, wife of Ricard
	name = "Cora"
	dynasty = dynasty_gabalaire
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = humble
	trait = shy
	trait = forgiving
	trait = depressed_1
	trait = education_learning_2

	968.4.30 = {
		birth = yes
	}
	
	985.11.26 = {
		add_spouse = 7003
	}
	
	1021.12.31 = {
		employer = 7002
	}
}

damerian7005 = { # William I "the Westward" of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = diligent
	trait = brave
	trait = calm
	trait = education_martial_4
	trait = unyielding_defender
	trait = strategist
	trait = overseer
	
	861.3.12 = {
		birth = yes
	}
	
	898.5.2 = {
		give_nickname = nick_the_westward
	}
	
	915.12.20 = {
		death = "915.12.20"
	}
}

damerian7006 = { # Caylen of Wesdam
	name = "Caylen"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = temperate
	trait = generous
	trait = humble
	trait = education_stewardship_4
	trait = administrator
	
	father = damerian7005
	
	878.7.18 = {
		birth = yes
	}
	
	934.2.23 = {
		death = "934.2.23"
	}
}

damerian7007 = { # William II of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = fickle
	trait = gregarious
	trait = gluttonous
	trait = education_diplomacy_2
	
	father = damerian7006
	
	900.5.31 = {
		birth = yes
	}
	
	945.3.26 = {
		death = "945.3.26"
	}
}

damerian70071 = { # Moruith of Wesdam, wife of Arrel Dameris
	name = "Moruith"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = twin
	
	father = damerian7006
	
	908.7.27 = {
		birth = yes
	}
	
	970.6.20 = {
		death = "970.6.20"
	}
}

damerian70072 = { # Milian of Wesdam, wife of Rogec Dameris
	name = "Milian"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = twin
	
	father = damerian7006
	
	908.7.27 = {
		birth = yes
	}
	
	980.9.10 = {
		death = "980.9.10"
	}
}

damerian7008 = { # Devan of Wesdam
	name = "Devan"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = cynical
	trait = callous
	trait = stubborn
	trait = education_learning_1
	
	father = damerian7007
	
	919.1.18 = {
		birth = yes
	}
	
	974.3.25 = {
		death = "974.3.25"
	}
}

damerian7009 = { # William III of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = honest
	trait = diligent
	trait = forgiving
	trait = education_stewardship_2
	
	father = damerian7008
	
	943.10.6 = {
		birth = yes
	}
	
	990.11.25 = {
		death = {
			death_reason = death_consumption
		}
	}
}

damerian7010 = { # Petran, Trutgaud's father
	name = "Petran"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = arbitrary
	trait = cynical
	trait = ambitious
	
	father = damerian7008
	
	950.4.2 = {
		birth = yes
	}
	
	994.6.17 = { # Treachery at Tomansford
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

damerian7011 = { # Daran Sil Cestir
	name = "Daran"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_martial_3
	trait = education_martial_prowess_3
	trait = reckless
	trait = brave
	trait = impatient
	trait = arbitrary
	
	984.10.4 = {
		birth = yes
	}
	
	1002.7.4 = {
		add_spouse = damerian7015
	}
}

damerian7012 = { # Lain Sil Cestir, first chiled of Daran
	name = "Lain"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_diplomacy_2
	trait = temperate
	trait = compassionate
	trait = calm
	trait = one_legged
	
	father = damerian7011
	mother = damerian7015
	
	1004.1.8 = {
		birth = yes
	}
}

damerian7013 = { # Toman Sil Cestir, second child of Daran
	name = "Toman"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_intrigue_2
	trait = fickle
	trait = arrogant
	trait = callous
	
	father = damerian7011
	mother = damerian7015
	
	1007.5.7 = {
		birth = yes
	}
}

damerian7014 = { # Auci Sil Cestir, third child of Daran
	name = "Auci"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = curious
	trait = trusting
	trait = patient
	disallow_random_traits = yes
	
	father = damerian7011
	mother = damerian7015
	
	1010.2.14 = {
		birth = yes
	}
}

damerian7015 = { # Dalya Cannleis, Daran's wife
	name = "Dayla"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = generous
	trait = ambitious
	trait = cynical
	
	father = damerian7016
	
	987.5.28 = {
		birth = yes
	}
	
	1002.7.4 = {
		add_spouse = damerian7011
	}
}

damerian7016 = { # Godric Cannleis, count of Cannleigh
	name = "Godric"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_learning_1
	trait = content
	trait = lazy
	trait = craven
	
	957.2.2 = {
		birth = yes
	}
}

damerian7017 = { # Clarya Cannleis, sister of Dayla, died in childbirth
	name = "Clarya"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = education_stewardship_1
	trait = greedy
	trait = trusting
	trait = callous
	
	father = damerian7016
	
	983.7.27 = {
		birth = yes
	}
	
	999.9.21 = {
		# marries some dude, maybe a count near
	}
	
	1002.9.3 = {
		death = {
			death_reason = death_childbirth
		}
	}
}

damerian7018 = { # Duke William of Exwes
	name = "William"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	
	trait = race_human
	trait = education_diplomacy_2
	trait = gluttonous
	trait = content
	trait = forgiving
	
	982.10.23 = {
		birth = yes
	}
	
	1000.9.13 = {
		add_spouse = damerian7019
	}
}

damerian7019 = { # Margery of Hookfield. wife of William of Exwes
	name = "Margery"
	dynasty = dynasty_hookfield
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	trait = race_human
	trait = education_learning_2
	trait = calm
	trait = chaste
	trait = compassionate
	
	984.7.17 = {
		birth = yes
	}
	
	1000.9.13 = {
		add_spouse = damerian7018
	}
}

damerian7020 = { # Albert of Exwes, heir of Exwes
	name = "Albert"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	
	trait = race_human
	trait = education_stewardship_2
	trait = diligent
	trait = gregarious
	trait = stubborn
	
	father = damerian7018
	mother = damerian7019
	
	1002.11.15 = {
		birth = yes
	}
}

damerian7021 = { # Lisbet of Exwes, daughter of William of Exwes
	name = "Lisbet"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	trait = race_human
	trait = education_learning_2
	trait = honest
	trait = patient
	trait = stubborn
	
	father = damerian7018
	mother = damerian7019
	
	1004.4.20 = {
		birth = yes
	}
}

damerian7022 = { # Alena, lowborn who sired Albert of Exwes's kids
	name = "Alena"
	# lowborn
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = gregarious
	trait = brave
	trait = stubborn
	
	1002.8.6 = {
		birth = yes
	}
	
	1020.9.30 = {
		death = {
			death_reason = death_childbirth
		}
	}
}

damerian7023 = { # Arrel of Exwes, 1st son of Albert of Exwes
	name = "Arrel"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	
	trait = race_human
	trait = twin
	trait = legitimized_bastard
	
	father = damerian7020
	mother = damerian7022
	
	1020.9.30 = {
		birth = yes
		set_relation_guardian = character:damerian7021
	}
}

damerian7024 = { # Adela of Exwes, 2nd daughter of Albert of Exwes
	name = "Adela"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	father = damerian7020
	mother = damerian7022
	
	trait = race_human
	trait = twin
	trait = legitimized_bastard
	
	1020.9.30 = {
		birth = yes
		set_relation_guardian = character:damerian7021
	}
}
