k_bahar = {
	1000.1.1 = { change_development_level = 9 }
	998.1.1 = {
		holder = bulwari0001	#Hussam of Nisabat
	}
	1002.2.1 = {
		liege = "e_bulwar"
	}
}

c_aqatbar = {
	1000.1.1 = { change_development_level = 17 }
}

c_jesdbar = {
	1000.1.1 = { change_development_level = 7 }
}

d_magairous = {
	1000.1.1 = { change_development_level = 8 }
}

c_agshelum = {
	1000.1.1 = { change_development_level = 9 }
}

c_deshkumar = {
	1000.1.1 = { change_development_level = 11 }
}

c_fajabahar = {
	1000.1.1 = { change_development_level = 8 }
}

c_akal_uak = {
	1000.1.1 = { change_development_level = 13 }
}

c_azka_evran = {
	1000.1.1 = { change_development_level = 11 }
}

c_qasnabor = {
	1000.1.1 = { change_development_level = 7 }
}

d_azkasad = {
	1000.1.1 = { change_development_level = 7 }
}