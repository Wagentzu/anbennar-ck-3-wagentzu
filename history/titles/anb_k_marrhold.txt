k_marrhold = {
	1000.1.1 = { change_development_level = 8 }
	989.7.18 = {
		holder = 91 #Hoger Marr
	}
}

d_marrhold = {
	1000.1.1 = { change_development_level = 10 }
}

c_marrhold = {
	1000.1.1 = { change_development_level = 12 }
}

c_marrhyl = {
	1000.1.1 = { change_development_level = 8 }
}

c_willowmore = {
	1000.1.1 = { change_development_level = 7 }
}

c_pixiebury = {
	1000.1.1 = { change_development_level = 9 }
}

c_dryadsdale = {
	1000.1.1 = { change_development_level = 7 }
}