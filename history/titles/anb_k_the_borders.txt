k_the_borders = {
	1000.1.1 = { change_development_level = 5 }
}

c_arannen = {
	1000.1.1 = { change_development_level = 6 }
	1021.10.31 = {
		holder = 50016
		government = feudal_government
	}
}

c_endersby = {
	992.10.23 = {
		holder = 50017
	}
}

c_tellum = {
	1000.1.1 = { change_development_level = 6 }
}

d_celliande = {
	1021.10.31 = {
		holder = 50002
		government = feudal_government
	}
}

c_sorncell = {
	1000.1.1 = { change_development_level = 6 }
	1021.10.31 = {
		holder = 50002
	}
}

c_doveswalk = {
	1021.5.23 = {
		holder = 50003
	}
	1021.10.31 = {
		liege = d_celliande
	}
}

c_irmathmas = {
	1021.5.23 = {
		holder = 50003
	}
	1021.8.6 = {
		liege = d_celliande
	}
}

c_wisphollow = {
	1000.1.1 = { change_development_level = 1 }
}

# d_hawkfields = {
# 	800.1.1 = {
# 		liege = "k_the_borders"
# 	}
# }

c_hawkshot = {
	1021.5.23 = {
		holder = 169
	}
}

c_birchwhite = {
	1021.5.23 = {
		holder = 169
	}
}

d_toarnen = {
	1021.10.31 = {
		holder = 50000
		government = feudal_government
	}
}

c_bordercliff = {
	1021.10.31 = {
		holder = 50000
	}
}

c_aiscestir = {
	1021.10.31 = {
		holder = 50000
	}
}

c_cronesford = {
	992.10.23 = {
		holder = 50008
	}
}

c_marllin = {
	1021.10.31 = {
		holder = 50018
		government = feudal_government
	}
}

d_brinkwick = {
	989.4.14 = {
		holder = 50023
	}
}

c_brinkwick = {
	989.4.14 = {
		holder = 50023
	}
}

c_eastborders = {
	989.4.14 = {
		holder = 50023
	}
	1010.1.5 = {
		holder = 50026
		liege = d_brinkwick
	}
}

c_murkglade = {
	989.4.14 = {
		holder = 50023
	}
	1010.1.5 = {
		holder = 50025
		liege = d_brinkwick
	}
}

c_wisphollow = {
	989.4.14 = {
		holder = 50023
	}
}