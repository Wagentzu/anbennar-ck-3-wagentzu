
k_enteben = {
	1000.1.1 = { change_development_level = 8 }
	
	811.5.5 = {
		holder = caylentis0001
	}
	855.9.12 = {
		holder = caylen_caylentis
	}
	885.9.30 = {
		holder = caylentis0004
	}
	895.12.19 = {
		holder = lorentis0001
	}
	900.1.1 = { 	#war of the one rose
	 	holder = 0
	}
}


d_great_ording = {
	900.1.1 = { 	# The War of the One Rose
		liege = k_lorent
	}
	1001.07.03 = {
		holder = 160
	}
}

c_high_ording = {
	1001.07.03 = {
		holder = 160
	}
}

c_east_ording = {
	1001.07.03 = {
		holder = 160
	}
}

c_west_ording = {
	1001.07.03= {
		holder = 160
	}
}

d_horsegarden = {
	1000.1.1 = { change_development_level = 9 }
	900.1.1 = { 	#war of the one rose
		liege = k_lorent
	}
}

c_horsegarden = {
	900.1.1 = { 	# The War of the One Rose
		liege = k_lorent
	}
	1015.5.12 = {
		holder = 596	#Lunaor Gwevoris
	}
}

c_greatfield = {
	900.1.1 = { 	# The War of the One Rose
		liege = k_lorent
	}
	992.9.25 = {
		holder = 588	#Gawan Brannis
	}
}

d_crovania = {
	1000.1.1 = { change_development_level = 7 }
	900.1.1 = { 	#war of the one rose
		liege = k_lorent
	}
	1022.1.1 = {
		holder = 2	#temporarily until they get actual holders
	}
}

c_mistspear = {
	900.1.1 = { 	#war of the one rose
		liege = k_lorent
	}
	986.7.10 = {
		holder = 593	#Griselda Modrentis, Widow of Mistspear
	}
}

c_crovans_rest = {
	900.1.1 = { 	#war of the one rose
		liege = k_lorent
	}
	986.7.10 = {
		holder = 592	#Drunhem Modrentis
	}
}

c_new_adea = {
	900.1.1 = { 	#war of the one rose
		liege = k_lorent
	}
	989.7.3 = {
		holder = 597	#Belan Calonis
	}
}

d_enteben = {
	900.1.1 = { 	#war of the one rose
		liege = k_lorent
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_enteben = {
	900.1.1 = { 	# The War of the One Rose
		liege = k_lorent
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_saddleglade = {
	900.1.1 = { 	# The War of the One Rose
		liege = k_lorent
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_wagonwood = {
	900.1.1 = { 	# The War of the One Rose
		liege = k_lorent
	}
	992.9.25 = {
		holder = 588	#Gawan Brannis
	}
}