k_gawed = {
	1000.1.1 = { change_development_level = 8 }
	# 800.1.1 = {
		# succession_laws = { male_only_law }
	# }
	923.12.27 = {
		holder = 526 #Carlan Gawe inherits from his father
	}
	950.4.7 = {
		holder = 525 #Ulric Gawe
	}
	956.12.4 = {
		holder = 523 #Godrac Gawe
	}
	986.5.23 = {
		holder = 51 #Alenn Gawe
	}
}

d_vertesk = {
	1000.1.1 = { change_development_level = 9 }
}

c_vertesk = {
	1000.1.1 = { change_development_level = 17 }
}

c_alenath = {
	1000.1.1 = { change_development_level = 13 }
}

c_gaweton = {
	1000.1.1 = { change_development_level = 11 }
}

c_vanbury = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_gawed"
	}
	923.12.27 = {
		holder = 526 #Carlan Gawe inherits from his father
	}
	949.3.28 = {
		holder = 527 #Ricard of Vanbury gifted by his father for his talents
	}
	982.5.29 = {
		holder = 528 #Carlan of Vanbury
	}
	1015.4.10 = {
		holder = 529 #Godryc of Vanbury
	}
}

c_drakesford = {
	1000.1.1 = { change_development_level = 7 }
	800.1.1 = {
		liege = "k_gawed"
	}
	986.4.10 = {
		holder = alenic30030 # Garrett Drakesford
	}
	1015.12.14 = {
		holder = alenic30027 # Byron Drakesford
	}
}

c_baldfather = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_gawed"
	}

	1020.1.1 = {
		holder = alenic30033 # Godrac Baldfather
	}
}

c_swinthorpe = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_gawed"
	}

	1020.1.1 = {
		holder = alenic30033 # Godrac Baldfather
	}
}

c_gerwick = {
	1000.1.1 = { change_development_level = 10 }
}

c_exeham = {
	1000.1.1 = { change_development_level = 9 }
}

d_westmounts = {
	1018.4.19 = {
		holder = 98 #Toman Wight
		liege = "k_gawed"
	}
}

d_greatmarch = {
	1000.1.1 = { change_development_level = 6 }
}

c_greatwoods = {
	800.1.1 = {
		liege = "k_gawed"
	}
	986.4.10 = {
		holder = alenic30030 # Garrett Drakesford
	}
	1015.12.14 = {
		holder = alenic30027 # Byron Drakesford
	}
}

c_legions_clearing = {
	1000.1.1 = { change_development_level = 8 }
}

d_balvord = {
	1000.1.1 = { change_development_level = 6 }
	994.1.1 = {
		holder = 579 #Petran
		liege = "k_gawed"
	}
}

c_gardfort = {
	994.1.1 = {
		holder = 579 #Petran
		liege = "k_gawed"
	}
}

c_aldtempel = {
	994.1.1 = {
		holder = 579 #Petran
		liege = "k_gawed"
	}
}

c_wolfden = {
	994.1.1 = {
		holder = 579 #Petran
	}
	1021.6.14 = {
		holder = 581 #Jordan
		liege = "d_balvord"
	}
}

d_oudescker = {
	1000.1.1 = { change_development_level = 6 }
	800.1.01 = {
		liege = k_gawed
		}
	979.5.30 = {
		holder = alenic 30040 # Oswald Oudescker
	}
}

c_jonsway = {
	1000.1.1 = { change_development_level = 7 }
}

d_arbaran = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

c_arbaran = {
	1000.1.1 = { change_development_level = 10 }
}

c_arca_pirvar = {
	1000.1.1 = { change_development_level = 9 }
}

c_elvenaire = {
	1000.1.1 = { change_development_level = 7 }
}

c_mirewatch = {
	1000.1.1 = { change_development_level = 7 }
}

c_freecestir = {
	1000.1.1 = { change_development_level = 12 }
}
