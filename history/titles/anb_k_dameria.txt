k_dameria = {
	1000.1.1 = { change_development_level = 8 }

	# This should be done as a realm law rather than a title law
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	900.1.1 = { # placeholder
		holder = dameris0001
	}
	915.10.30 = {
		holder = dameris0002
	}
	940.2.2 = {
		holder = dameris0003
	}
	955.10.2 = {
		holder = 25 #Galien Dameris
	}
	980.12.1 = {
		holder = 13	#Auci
	}
	1021.10.3 = {
		holder = damerian0001	#Marion
	}
}


d_damesear = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_dameria"
	}
	900.1.1 = { # placeholder
		holder = dameris0001
	}
	915.10.30 = {
		holder = dameris0002
	}
	940.2.2 = {
		holder = dameris0003
	}
	955.10.2 = {
		holder = 25 #Galien Dameris
	}
	980.12.1 = {
		holder = 13	#Auci
	}
	1021.10.3 = {
		holder = damerian0001
	}
}

c_varivar = {
	1000.1.1 = { change_development_level = 9 }
	800.1.1 = {
		liege = "k_dameria"
	}
	1003.7.3 = {
		holder = 518	#Varilor Bluetongue
		effect = {
			add_quest_to_county = {
				QUEST = goblins_infestation
			}
		}
	}
}

c_eargate = {
	1021.10.3 = {
		holder = damerian0001
	}
}

c_anbenncost = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.3 = {
		holder = damerian0001
		effect = {
			add_quest_to_county = {
				QUEST = goblins_infestation
			}
		}
	}
}

c_old_damenath = {
	1000.1.1 = { change_development_level = 13 }
	1021.10.3 = {
		holder = damerian0001
	}
}

c_damesteeth = {
	1000.1.1 = { change_development_level = 6 }
	1021.10.3 = {
		holder = damerian0001
	}
}

c_moonmount = {
	1000.1.1 = { change_development_level = 7 }
	980.12.1 = {
		holder = 13	#Auci
	}
	1021.10.3 = {
		holder = damerian0001
	}
}

c_auraire = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.3 = {
		holder = damerian0001
	}
}


d_wesdam = {
	898.5.2 = {
		liege = "k_dameria"
		holder = damerian7005 # William I
	}
	915.12.20 = {
		liege = "k_dameria"
		holder = damerian7006 # Caylen I
	}
	934.2.23 = {
		liege = "k_dameria"
		holder = damerian7007 # William II
	}
	945.3.26 = {
		liege = "k_dameria"
		holder = damerian7008 # Devan I
	}
	974.3.25 = {
		liege = "k_dameria"
		holder = damerian7009 # William III
	}
	990.11.25 = {
		liege = "k_dameria"
		holder = 7003 # Ricard I
	}
	1014.7.24 = {
		liege = "k_dameria"
		holder = 7000 # Westyn I
	}
	1018.4.13 = {
		liege = "k_dameria"
		holder = 7002 # Westyn II
	}
}

c_wesdam = {
	898.5.2 = {
		liege = "k_dameria"
		holder = damerian7005 # William I
	}
	915.12.20 = {
		liege = "k_dameria"
		holder = damerian7006 # Caylen I
	}
	934.2.23 = {
		liege = "k_dameria"
		holder = damerian7007 # William II
	}
	945.3.26 = {
		liege = "k_dameria"
		holder = damerian7008 # Devan I
	}
	974.3.25 = {
		liege = "k_dameria"
		holder = damerian7009 # William III
	}
	990.11.25 = {
		liege = "k_dameria"
		holder = 7003 # Ricard I
	}
	1000.1.1 = { change_development_level = 10 }
	1014.7.24 = {
		liege = "k_dameria"
		holder = 7000 # Westyn I
	}
	1018.4.13 = {
		liege = "k_dameria"
		holder = 7002 # Westyn II
	}
}

c_lenceiande = {
	898.5.2 = {
		liege = "k_dameria"
		holder = damerian7005 # William I
	}
	915.12.20 = {
		liege = "k_dameria"
		holder = damerian7006 # Caylen I
	}
	934.2.23 = {
		liege = "k_dameria"
		holder = damerian7007 # William II
	}
	945.3.26 = {
		liege = "k_dameria"
		holder = damerian7008 # Devan I
	}
	974.3.25 = {
		liege = "k_dameria"
		holder = damerian7009 # William III
	}
	990.11.25 = {
		liege = "k_dameria"
		holder = 7003 # Ricard I
	}
	1000.1.1 = { change_development_level = 10 }
	1014.7.24 = {
		liege = "k_dameria"
		holder = 7000 # Westyn I
	}
	1018.4.13 = {
		liege = "k_dameria"
		holder = 7002 # Westyn II
	}
}

d_exwes = {
	998.7.8 = {
		liege = k_dameria
		holder = damerian7018 # William of Exwes
	}
}

d_istralore = {
	1000.1.1 = { change_development_level = 8 }
	800.1.1 = {
		liege = "k_dameria"
	}
	
	1021.1.4 = {
		holder = 19	#Istralania Warsinger
	}
}

c_istralore = {
	1000.1.1 = { change_development_level = 9 }
	800.1.1 = {
		liege = "d_istralore"
	}

	1021.1.4 = {
		holder = 19 #Istralania Warsinger
	}
}

c_damesdale = {
	1000.1.1 = { change_development_level = 8 }
}

c_ricancas = {
	1000.1.1 = { change_development_level = 8 }
	800.1.1 = {
		liege = "d_istralore"
	}

	1021.1.4 = {
		holder = 37 #Sideric
	}
}


d_neckcliffe = {
	1000.1.1 = { change_development_level = 8 }
	800.1.1 = {
		liege = "k_dameria"
	}

	1013.2.16 = {
		holder = 544	#his father dies at the Battle of Damesteeth
	}
}

c_neckcliffe = {
	1000.1.1 = { change_development_level = 12 }
}

c_triancost = {
	1000.1.1 = { change_development_level = 9 }
}


d_acromton = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_dameria"
	}

	980.12.1 = {
		holder = 500
	}
	993.12.4 = {
		holder = 40 #Acromar, son of Acromar (500) was a damerian loyalist and killed by Sorcerer-King
	}
	999.7.4 = {	
		holder = 39 #his successor, his brother, is not a loyalist
	}
}

c_acromton = {
	1000.1.1 = { change_development_level = 12 }
	800.1.1 = {
		liege = "d_acromton"
	}

	980.12.1 = {
		holder = 500
	}
	993.12.4 = {
		holder = 40 #Acromar, son of Acromar (500) was a damerian loyalist and killed by Sorcerer-King
	}
	999.7.4 = {	
		holder = 39 #his successor, his brother, is not a loyalist
	}
}

c_damescross = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "d_acromton"
	}

	980.12.1 = {
		holder = 500
	}
	993.12.4 = {
		holder = 40 #Acromar, son of Acromar (500) was a damerian loyalist and killed by Sorcerer-King
	}
	999.7.4 = {	
		holder = 39 #his successor, his brother, is not a loyalist
	}
}


d_plumwall = {
	1000.1.1 = { change_development_level = 10 }
}

c_plumwall = {
	1000.1.1 = { change_development_level = 11 }
}

c_taxwick ={
	1000.1.1 = { change_development_level = 12 }
}

c_silvelar = {
	1000.1.1 = { change_development_level = 13 }
}


#d_upper_luna

c_cestaire = {
	1001.1.1 = { change_development_level = 12 }
	
	1003.12.8 = {
		holder = damerian7011
	}
	
	1004.2.3 = {
		liege = "k_dameria"
	}
}

c_lancecastle = {
	1000.1.1 = { change_development_level = 9 }
	
	1003.12.8 = {
		holder = damerian7011
	}
	
	1004.2.3 = {
		liege = "k_dameria"
	}
}

c_aranthil = {
	1000.1.1 = { change_development_level = 11 }
	800.1.1 = {
		liege = "k_dameria"
	}
	1021.1.4 = {
		holder = 18 #Garion Silgarion
	}
}


#d_heartlands

c_dhaneir = {
	1000.1.1 = { change_development_level = 14 }
}

c_elensbridge = {
	1000.1.1 = { change_development_level = 12 }
}

c_byshade = {
	1000.1.1 = { change_development_level = 9 }
}

c_palemarket = {
	1000.1.1 = { change_development_level = 11 }
}

c_cannleigh = {
	1000.1.1 = { change_development_level = 8 }
	
	990.4.2 = {
		holder = damerian7016
	}
	
	1004.2.3 = {
		liege = "k_dameria"
	}
}


d_silverwoods = {
	1000.1.1 = { change_development_level = 6 }
	800.1.1 = {
		liege = "k_dameria"
	}
	1021.10.31 = {
		holder = 518 #Varilor Bluetongue
		government = feudal_government
		succession_laws = { elven_elective_succession_law elf_only }
	}
}

c_moonhaven = {
	1000.1.1 = { change_development_level = 8 }
}
