#k_roysfort
##d_roysfort
###c_roysfort
131 = {		#Roysfort

    # Misc
    culture = roysfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1330 = {

    # Misc
    holding = none

    # History

}
1329 = {

    # Misc
    holding = none

    # History

}
1331 = {

    # Misc
    holding = castle_holding

    # History

}

###c_smallmarches
130 = {		#Smallmarches

    # Misc
    culture = roysfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1336 = {

    # Misc
    holding = none

    # History

}

###c_greenberry
132 = {		#Greenberry

    # Misc
    culture = roysfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1333 = {

    # Misc
    holding = none

    # History

}
1334 = {

    # Misc
    holding = city_holding

    # History

}
1335 = {

    # Misc
    holding = none

    # History

}

##d_bigwheat
###c_bigwheat
136 = {		#Bigwheat

    # Misc
    culture = roysfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1249 = {

    # Misc
    holding = city_holding

    # History

}
1288 = {

    # Misc
    holding = none

    # History

}
1283 = {

    # Misc
    holding = church_holding

    # History

}
1318 = {

    # Misc
    holding = none

    # History

}
1328 = {

    # Misc
    holding = none

    # History

}
1273 = {

    # Misc
    holding = none

    # History

}

###c_middlewood
134 = {		#Middlewood

    # Misc
    culture = roysfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History
}
1332 = {

    # Misc
    holding = none

    # History

}
