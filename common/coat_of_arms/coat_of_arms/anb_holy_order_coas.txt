﻿ho_knights_of_the_saltmarch = {
	pattern = "pattern_solid.dds"
	color1 = "white"
	color2 = "black"	
	colored_emblem = {
		texture = "ce_block_02.dds"
		color1 = "black"
		color2 = "black"
		instance = { position = { 0.5 0.128 } scale = { 1.0 0.256 } }
	}	
	colored_emblem = {
		texture = "ce_anb_sword.dds"
		color1 = "red"
		color2 = "blue"
		instance = { position = { 0.5 0.58 } scale = { 0.7 0.7 } }						
	}
}