﻿
# Anbennar - racial compatibility

remove_guardian_interaction_visible_ward_trigger = {
	OR = {
		#If I am the child
		this = scope:actor
		#The ward has actor as its liege
		AND = {
			exists = employer
			employer = scope:actor
		}
		#The guardian is actor
		any_relation = {
			type = guardian
			this = scope:actor
		}
		#The guardian is actor's courtier
		any_relation = {
			type = guardian
			is_courtier_of = scope:actor
		}
	}
}

remove_guardian_interaction_available_ward_trigger = {
	is_busy_in_events_localised = yes
	#is_imprisoned = no
}

kick_from_court_validity_trigger = {
	is_busy_in_events_localised = yes
	NOR = {
		is_spouse_of = scope:actor
		is_concubine_of = scope:actor
		is_close_family_of = scope:actor
		has_council_position = councillor_court_chaplain
	}
}

ask_for_pardon_available_trigger = {
	$ACTOR$ = {
		is_independent_ruler = no
		any_liege_or_above = {
			OR = {
				has_imprisonment_reason = $ACTOR$
				has_banish_reason = $ACTOR$
				has_execute_reason = $ACTOR$
				has_revoke_title_reason = $ACTOR$
			}
		}
	}
}

title_revocation_is_tyrannical_trigger = {
	NOR = {
		has_revoke_title_reason = scope:recipient
		has_claim_on = scope:landed_title
		faith = {
			scope:recipient = {
				NOT = { vassal_contract_has_flag = religiously_protected}
			}
			OR = {
				AND = {								
					has_doctrine_parameter = pluralism_righteous_revocation_tyranny_reduced
					faith_hostility_level = {
						target = scope:recipient.faith
						value = faith_evil_level
					}
				}
				AND = {								
					has_doctrine_parameter = pluralism_fundamentalist_revocation_tyranny_minimal
					faith_hostility_level = {
						target = scope:recipient.faith
						value >= faith_hostile_level
					}
				}
			}
		}
		# Anbennar - Racial legitimacy
		character_is_illegitimate_race_for_title = {
			TITLE = scope:landed_title
			CHARACTER = scope:recipient
		} 
	}
}

vassal_revocation_is_tyrannical_trigger = {
	NOR = {
		has_revoke_title_reason = scope:recipient
		faith = {
			OR = {
				AND = {								
					has_doctrine_parameter = pluralism_righteous_revocation_tyranny_reduced
					faith_hostility_level = {
						target = scope:recipient.faith
						value = faith_evil_level
					}
				}
				AND = {								
					has_doctrine_parameter = pluralism_fundamentalist_revocation_tyranny_minimal
					faith_hostility_level = {
						target = scope:recipient.faith
						value >= faith_hostile_level
					}
				}
			}
		}
	}
}

is_de_jure_vassal_of_liege_trigger = {
	liege = {
		save_temporary_scope_as = my_liege
	}
	primary_title = {
		OR = {	
			de_jure_liege = {
				exists = holder
				holder = scope:my_liege
			}
			trigger_if = {
				limit = {
					tier = tier_duchy
				}
				de_jure_liege = {		
					de_jure_liege = {
						exists = holder
						holder = scope:my_liege
					}
				}
			}
		}
	}
}

send_poem_positive_poem_lock_trigger = {
	OR = {
		is_ai = no
		NOT = { has_relation_rival = scope:recipient }
		NOT = { has_relation_nemesis = scope:recipient }
	}
}

send_poem_negative_poem_lock_trigger = {
	OR = {
		is_ai = no
		has_relation_rival = scope:recipient
		has_relation_nemesis = scope:recipient
	}
}


can_any_traveling_family_members_travel_trigger = {
	NOR = {
		this = scope:recipient
		is_in_pool_at = scope:actor.capital_province
	}
	is_ruler = no
	trigger_if = {
		limit = {
			exists = scope:recipient.host
			is_child_of = scope:recipient.host
			is_adult = no
		}
		NOT = { dynasty = scope:recipient.host.dynasty }
	}
}

grant_title_rivalry_trigger = {
	is_ai = yes
	is_landed = yes
	top_liege = scope:actor
	faith = scope:actor.faith
	reverse_opinion = {
		target = top_liege
		value > -50
	}
	opinion = {
		target = top_liege
		value > -50
	}
	trigger_if = {
		limit = {
			is_female = yes 
			faith_dominant_gender_female_or_equal = no
		}
		scope:recipient = { is_female = yes }
	}
	trigger_if = {
		limit = {
			is_male = yes 
			faith_dominant_gender_male_or_equal = no
		}
		scope:recipient = { is_male = yes }
	}
	has_dread_level_towards = {
		target = scope:actor
		level < 1
	}
	NOR = {
		this = scope:actor
		this = scope:recipient
		has_trait = content
		has_trait = humble
		has_trait = blind
		has_trait = devoted
		has_trait = eunuch
		is_close_family_of = scope:recipient
		has_any_good_relationship_with_character_trigger = { CHARACTER = scope:actor }
		has_any_bad_relationship_with_character_trigger = { CHARACTER = scope:actor }
	}
}
