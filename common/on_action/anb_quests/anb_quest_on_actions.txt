﻿on_goblin_adventure_maintenance = {
}

on_goblin_adventure_maintenance_events = {
	random_events = {
		chance_to_happen = 10
		
		# Generic quest events
		100 = anb_generic_quest_events.0001 # Adventurer insults vassal
		5 = anb_generic_quest_events.0002 # Landowner expels adventurers
		100 = anb_generic_quest_events.0003 # Adventurers bring business
		100 = anb_generic_quest_events.0004 # Exploting the adventurers?
		100 = anb_generic_quest_events.0005 # Adventurers damage province
		100 = anb_generic_quest_events.0006 # Adventurer goes to tavern instead of adventuring
		100 = anb_generic_quest_events.0009 # Local garrison is a nuisance
		
		100 = anb_goblin_quest.0003 # Find goblin lair
		100 = anb_goblin_quest.0004 # Confrontation with goblins
		100 = anb_goblin_quest.0005 # Confrontation with goblin boss
		100 = anb_goblin_quest.0006 # Goblins fortify lair
		100 = anb_goblin_quest.0007 # Train peasants against goblins
		100 = anb_goblin_quest.0008 # Goblins request to settle
		100 = anb_goblin_quest.0009 # Peasants use goblins as slaves
		100 = anb_goblin_quest.0010 # Goblin treasure found
		100 = anb_goblin_quest.0011 # Local garrison slaughters goblins
	}
}