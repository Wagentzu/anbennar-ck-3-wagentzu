﻿
tax_collector_character = {
	age = { 30 65 }
	gender_female_chance = root_faith_dominant_gender_female_chance
	random_traits = yes
	culture = scope:county.culture
	faith = scope:county.faith
	stewardship = {
		min_template_decent_skill
		max_template_decent_skill
	}
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
	}
}