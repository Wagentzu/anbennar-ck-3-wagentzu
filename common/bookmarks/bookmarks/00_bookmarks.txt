﻿bm_2000_test = {
	start_date = 1022.1.1
	is_playable = yes
	group = bm_group_1

	character = {
		name = "bookmark_test_marion_silmuna"
		dynasty = dynasty_silmuna
		dynasty_splendor_level = 1
		type = male
		birth = 1001.2.19
		title = k_dameria
		government = feudal_government
		culture = damerian
		religion = cult_of_the_dame
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = damerian0001
		position = { 900 530 }

		animation = disapproval
	}

	# character = {
		# name = "bookmark_test_maugun_of_ording"
		# history_id = 160
		# dynasty = dynasty_ording
		# dynasty_splendor_level = 1
		# type = male
		# birth = 981.04.24
		# title = d_great_ording
		# government = feudal_government
		# culture = lorenti
		# religion = court_of_adean
		# difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		# position = { 400 530 }
	# }
}

